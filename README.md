
### What is this repository for? ###

* Raisin Code Challenge

### How do I get set up? ###

* Require Java 1.8 for lambdas
* Program assumes host as localhost running on 7729 port (Change PubSubApplication.Java to change serverhost or port)
* clone the repo and run steps to follow
* 1) mvn clean compile
* 2) mvn install
* 3) python fixture.py
* 4) java -jar target/raisin_challenge-1.0-SNAPSHOT-jar-with-dependencies.jar